package com.lebroncraft.build.command.rotation;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandModule;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.Util;
import com.lebroncraft.core.common.util.UtilJava;
import com.lebroncraft.core.rank.Rank;
import com.lebroncraft.minigame.game.GameType;


public class CommandRotationRemove
		extends CommandModule<RotationManager> {
		
	public CommandRotationRemove(RotationManager manager) {
		super(manager, Rank.ADMIN, "remove", "delete");
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		if (args.size() < 1) {
			C.pMain(player, "Rotation", "Please specify a gametype");
			return;
		}
		
		String typeString = args.get(0);
		GameType type = UtilJava.parseEnum(GameType.class, typeString);
		if (type == null) {
			C.pMain(player, "Rotation", "Invalid gametype \"" + typeString + "\"");
			return;
		}
		
		String map = player.getWorld()
				.getName();
		if (args.size() > 1) {
			map = Util.joinList(args, " ", 1);
		}
		
		plugin.removeMap(type, map, player);
	}
	
}
