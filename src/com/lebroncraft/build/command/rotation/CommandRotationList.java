package com.lebroncraft.build.command.rotation;


import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.bukkit.entity.Player;

import com.google.common.collect.SetMultimap;
import com.lebroncraft.core.command.CommandModule;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.Util;
import com.lebroncraft.core.common.util.UtilJava;
import com.lebroncraft.core.common.util.UtilString;
import com.lebroncraft.core.rank.Rank;
import com.lebroncraft.minigame.game.GameType;


/**
 * Lists game rotation for the specified gametype. If gametype is not specified, lists rotation for
 * all gametypes.
 */
public class CommandRotationList
		extends CommandModule<RotationManager> {
		
	public CommandRotationList(RotationManager manager) {
		super(manager, Rank.ADMIN, "list", "display", "show");
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		GameType type = null;
		if (args.size() > 0) {
			String typeString = Util.joinList(args, " ");
			type = UtilJava.parseEnum(GameType.class, typeString);
			if (type == null) {
				C.pMain(player, "Rotation", "Gametype \"" + typeString + "\" is not a valid gametype");
				return;
			}
		}
		
		C.pRaw(player, "");
		
		if (type == null) {
			for (GameType gameType : GameType.values()) {
				listRotation(player, gameType);
			}
		} else {
			listRotation(player, type);
		}
	}
	
	private SetMultimap<GameType, String> getRotation() {
		return plugin.getMapManager()
				.getMapRotation();
	}
	
	private void listRotation(Player player, GameType type) {
		String name = UtilString.format(type.getName());
		
		C.pRaw(player, C.t1 + C.fDash(C.cGray + "Rotation", C.cPink + name));
		Set<String> maps = getRotation().get(type);
		if (maps == null || maps.isEmpty()) {
			C.pRaw(player, C.cRed + "No maps");
		} else {
			String mapMessage = C.cGreen;
			
			Iterator<String> mapsIt = maps.iterator();
			while (mapsIt.hasNext()) {
				mapMessage += mapsIt.next();
				if (mapsIt.hasNext()) {
					mapMessage += ", ";
				}
			}
			
			C.pRaw(player, mapMessage);
		}
		
		C.pRaw(player, "");
	}
	
}
