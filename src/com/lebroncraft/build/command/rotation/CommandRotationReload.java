package com.lebroncraft.build.command.rotation;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandModule;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.rank.Rank;


public class CommandRotationReload
		extends CommandModule<RotationManager> {
		
	public CommandRotationReload(RotationManager manager) {
		super(manager, Rank.ADMIN, "reload", "refresh");
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		plugin.getMapManager()
				.loadMapRotation();
		C.pMain(player, "Rotation", "Rotation reloaded");
	}
	
}
