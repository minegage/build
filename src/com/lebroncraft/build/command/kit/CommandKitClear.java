package com.lebroncraft.build.command.kit;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandModule;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.rank.Rank;


public class CommandKitClear
		extends CommandModule<MobkitManager> {
		
	public CommandKitClear(MobkitManager manager) {
		super(manager, Rank.BUILDER, "clear");
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		boolean success = plugin.getFile(player.getWorld())
				.delete();
				
		if (success) {
			C.pMain(player, "Kit", "Cleared all mobkits");
		} else {
			C.pMain(player, "Kit", "Unable to clear mobkits");
		}
		
	}
	
}
