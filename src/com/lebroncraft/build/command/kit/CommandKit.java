package com.lebroncraft.build.command.kit;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandModule;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.rank.Rank;


public class CommandKit
		extends CommandModule<MobkitManager> {
		
	public CommandKit(MobkitManager manager) {
		super(manager, Rank.BUILDER, "kit", "k");
		
		addSubCommand(new CommandKitCreate(manager));
		addSubCommand(new CommandKitDelete(manager));
		addSubCommand(new CommandKitClear(manager));
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		C.pHelp(player, "kit add/create <kit name> [mob type]");
		C.pHelp(player, "kit remove/delete <kit name>");
		C.pHelp(player, "kit clear");
	}
	
}
