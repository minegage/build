package com.lebroncraft.build.command.map;


import java.util.List;

import org.bukkit.World;
import org.bukkit.entity.Player;

import com.lebroncraft.build.BuildManager;
import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.rank.Rank;


public class CommandMapSetup
		extends CommandBase {
		
	private BuildManager manager;
	
	public CommandMapSetup(BuildManager manager) {
		super(Rank.ADMIN, "setup", "s");
		
		this.manager = manager;
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		World world = player.getWorld();
		manager.applySettings(world);
		
		C.pMain(player, "Map", "Setup complete");
	}
	
	
	
}
