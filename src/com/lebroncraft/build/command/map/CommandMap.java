package com.lebroncraft.build.command.map;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.build.BuildManager;
import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.rank.Rank;


public class CommandMap
		extends CommandBase {
		
	public CommandMap(BuildManager manager) {
		super(Rank.ADMIN, "map");
		
		addSubCommand(new CommandMapSetup(manager));
		addSubCommand(new CommandMapGenerate());
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
	
	}
	
}
