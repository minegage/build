package com.lebroncraft.build.command.map;


import java.util.List;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.Util;
import com.lebroncraft.core.common.util.UtilWorld;
import com.lebroncraft.core.rank.Rank;


public class CommandMapGenerate
		extends CommandBase {
		
	public CommandMapGenerate() {
		super(Rank.BUILDER, "generate", "gen");
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		if (args.size() < 1) {
			C.pMain(player, "Map", "Please specify a map name");
			return;
		}
		
		String mapName = Util.joinList(args, " ");
		if (UtilWorld.isLoadable(mapName)) {
			C.pMain(player, "Map", "That world already exists!");
			return;
		}
		
		C.pMain(player, "Map", "Generating map...");
		
		WorldCreator creator = WorldCreator.name(mapName)
				.type(WorldType.FLAT)
				.generatorSettings("1;0")
				.generateStructures(false);
		World world = creator.createWorld();
		world.getBlockAt(0, 0, 0)
				.setType(Material.BEDROCK);
				
		C.pMain(player, "Map", "Map generated.");
	}
	
}
