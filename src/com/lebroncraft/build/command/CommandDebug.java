package com.lebroncraft.build.command;


import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.log.L;
import com.lebroncraft.core.rank.Rank;


public class CommandDebug
		extends CommandBase {
		
	public CommandDebug() {
		super(Rank.ADMIN, "bd");
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		int i = Integer.parseInt(args.get(0));
		
		if (i == 0) {
			L.d(Bukkit.getWorldContainer()
					.getAbsolutePath());
		}
		
	}
	
}
