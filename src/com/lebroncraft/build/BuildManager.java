package com.lebroncraft.build;


import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.lebroncraft.build.command.CommandDebug;
import com.lebroncraft.build.command.kit.MobkitManager;
import com.lebroncraft.build.command.map.CommandMap;
import com.lebroncraft.build.command.rotation.RotationManager;
import com.lebroncraft.core.module.PluginModule;
import com.lebroncraft.minigame.command.CommandData;


public class BuildManager
		extends PluginModule {
		
	private RotationManager rotationManager;
	private MobkitManager mobkitManager;
	
	public BuildManager(JavaPlugin plugin) {
		super("Build Manager", plugin);
		
		Bukkit.setDefaultGameMode(GameMode.CREATIVE);
		this.rotationManager = new RotationManager(plugin);
		
		registerCommand(new CommandData());
		registerCommand(new CommandMap(this));
		registerCommand(new CommandDebug());
		
		this.mobkitManager = new MobkitManager(plugin);
		
	}
	
	public RotationManager getRotationManager() {
		return rotationManager;
	}
	
	public MobkitManager getMobkitManager() {
		return mobkitManager;
	}
	
	@EventHandler
	public void onWeatherChange(WeatherChangeEvent event) {
		if (event.toWeatherState()) {
			event.setCancelled(true);
		}
	}
	
	public void applySettings(World world) {
		world.setThundering(false);
		world.setStorm(false);
		world.setWeatherDuration(Integer.MAX_VALUE);
		
		world.setGameRuleValue("doDaylightCycle", "false");
		world.setGameRuleValue("doMobSpawning", "false");
		
		world.setTime(6000L);
	}
	
}
