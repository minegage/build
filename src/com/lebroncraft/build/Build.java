package com.lebroncraft.build;


import com.lebroncraft.core.CorePlugin;


public class Build
		extends CorePlugin {
		
	private BuildManager buildManager;
	
	@Override
	public void onEnable() {
		super.onEnable();
		
		this.buildManager = new BuildManager(this);
		
		getChatManager().setFilterEnabled(false);
		getEventManager().getSpawnManager().overrideSpawns = false;
	}
	
	public BuildManager getBuildManager() {
		return buildManager;
	}
	
}
